/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ListView,
  ScrollView,
  Picker,
  Switch
} from 'react-native';

const Contacts = require('react-native-contacts');
import ChatConfirmScreen from '../screens/ChatConfirmScreen';
import Api from '../api';

class ChatConfirmAndroid extends Component {

  constructor(props, context) {
    super(props);
    this.state = {
      contacts: [],
      eventDate: '',
      data: this.groupContacts(this.props.contacts)
    };

    this.navigateGroupEdit = this.navigateGroupEdit.bind(this);
    this.createEvent = this.createEvent.bind(this);
    this.groupContacts =this.groupContacts.bind(this);
    this.onContactsSelect = this.onContactsSelect.bind(this);
  }

  componentDidMount(){

    this.timeToDateFormat(this.props.time);

    Contacts.getAll((err, contacts) => {
      if(err && err.type === 'permissionDenied'){
        console.log('Permission Denied!');
      } else {

        this.setState({
          data:this.groupContacts(contacts)
        });
      }
    })
  }

  groupContacts (contacts) {

    const out = {};

    contacts = contacts.slice().sort((a, b) => {
      if (a.givenName < b.givenName) return -1
      if (a.givenName > b.givenName) return 1
      return 0
    })

    contacts.forEach(contact => {

      const letter = contact.givenName.substr(0, 1)

      out[letter] = out[letter] || []
      out[letter].push(contact)
    })

    return out
  }

  navigateGroupEdit = (data) => {
    this.props.navigator.push({
      ident:"ViewGroup",
      group:data,
      routeTitle : data
    })
  };

  createEvent = ()=> {

    let data = {
      time: this.state.eventDate,
      template: {
        title: this.props.title,
        category: this.props.category,
        imageSrc: this.props.image
      },
      contacts: this.state.contacts
    };

    Api.createEvent(data);

    this.props.navigator.push({
      ident:"MainScreen"
    })
  };


  onContactsSelect (contacts) {
    this.state.contacts = contacts;
  }

  render() {
    return(
    <ChatConfirmScreen
      navigateGroupEdit = {(data)=>this.navigateGroupEdit(data)}
      createEvent = {()=>this.createEvent()}
      contacts = {this.state.data}
      onCellSelect = {(contacts)=>this.onContactsSelect(contacts)}
    />
    )
  }
}

module.exports = ChatConfirmAndroid;