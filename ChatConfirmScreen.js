/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ListView,
  ScrollView,
  Picker,
  Switch
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
const imgDirPath =require("../img/jacket.jpg");
var AlphabetListView = require('react-native-alphabetlistview');

class Cell extends Component {

  state = {
    selected: false
  };

  onSelect () {
    const selected = !this.state.selected;
    this.props.onSelect(this.props.item, selected);
    this.setState({selected})
  }

  render () {
    const iconProps = {
      name: this.state.selected ? 'check-circle' : 'plus-circle',
      color: this.state.selected ? 'green' : '#ccc'
    };

    return (
      <View style={{marginHorizontal: 15, height: 45, justifyContent: 'center',  flexDirection: 'row'}}>
        <View style={{flex: 1}}>
          <Text>{this.props.item.title}</Text>
          <Text style={{color: 'grey'}}>{this.props.item.givenName}</Text>
        </View>
        <View>
          <TouchableOpacity onPress={() => this.onSelect()}>
            <Icon size={30} {...iconProps} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

class SectionItem extends Component {
  render() {
    return (
      <Text style={{color:'#000'}}>{this.props.title}</Text>
    );
  }
}


class SectionHeader extends Component {

  render() {
    var textStyle = {
      height: 25
    };

    var viewStyle = {
      backgroundColor: '#eee',
      paddingHorizontal: 15
    };
    return (
      <View style={viewStyle}>
        <Text style={textStyle}>{this.props.title}</Text>
      </View>
    )
  }
}

class SwitchComponent extends Component {
  constructor(props, context) {
    super(props, context);
    this.state={
      falseSwitchIsOn:false
    }
  }
  render(){
    return(
      <Switch
        onValueChange={(value) => this.setState({falseSwitchIsOn: value})}
        thumbTintColor="orange"
        tintColor="orange"
        value={this.state.falseSwitchIsOn} />
    );
  }
}

class ChatConfirmScreen extends Component {

  constructor(props, context) {
    super(props, context);
    this.renderRow = this.renderRow.bind(this);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state={
      selectedContacts: [],
      falseSwitchIsOn:false,
      dataSource: ds.cloneWithRows(this.props.data)
    }
    this.onCellSelect = this.onCellSelect.bind(this)
  }


  onCellSelect (item, selected) {
    const selectedContacts = this.state.selectedContacts;

    if (selected) {
      selectedContacts.push(item)
    } else {
      selectedContacts.splice(selectedContacts.indexOf(item), 1)
    }

    this.props.onCellSelect(selectedContacts);
    this.setState({selectedContacts});
  }


  renderRow(data){
    var switchStatus={
      value:false
    };

    return(
      <View style={styles.rowWrapper}>
        <TouchableOpacity onPress={(event)=>this.props.navigateGroupEdit(data.title)}>
          <View style ={styles.fullWidth}>
            <View style={styles.inlineWrapper}>
              <Image style={styles.image} source={imgDirPath} ></Image>
              <View style={styles.titleWrapper}>
                <Text style = {styles.mainText}>{data.title}</Text>
                <Text style = {styles.subText}>{data.friendsCount} friends</Text>
              </View>
              <Icon style = {styles.Icon} name="chevron-right" size={20} color="#d0d0d0" />
            </View>
            <View style={styles.switchWrapper}>
              <SwitchComponent/>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };
  

  render() {
    return (
      <View style={styles.View}>
        <View style={styles.listViewWrapper}>
          <Text style={styles.headerText}>My Groups</Text>
          <ListView
            contentContainerStyle={styles.list}
            dataSource={this.state.dataSource}
            renderRow={this.renderRow}
          />
        </View>
        <View style={styles.listViewWrapper}>
          <Text style={styles.headerTextIndivid}>Individual Pick</Text>
          <AlphabetListView
            automaticallyAdjustContentInsets={false}
            data={this.props.contacts}
            cell={Cell}
            cellHeight={30}
            sectionListItem={SectionItem}
            sectionHeader={SectionHeader}
            sectionHeaderHeight={25}
            sectionHeaderHeight={22.5}
            onCellSelect={this.onCellSelect}
          />
        </View>
        <TouchableOpacity  style = {styles.touchable} onPress={(event)=>this.props.createEvent()}>
          <View
            style={styles.btnBackground}
            elevation={1}
          >
            <Icon name="rocket"  size={30} color={'#fff'} />
            <Text style={styles.btnText}>NEXT</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  View: {
    marginTop: 80,
    backgroundColor: '#ffffff',
    flex:1,
    alignItems: 'center',
    height:height * .5
  },
  listViewWrapper: {
    flex:1
  },
  inlineIconView: {
    marginTop:30,
    flexDirection:'row',
    flexWrap:'wrap',
    alignItems: 'center',
    marginBottom:20
    // width:width
  },
  contactName: {
    fontFamily: 'OpenSans',
    fontWeight:'bold',
    color:'#000000',
    fontSize:20
  },
  inlineWrapper: {
    paddingLeft:5,
    width: width * .8,
    alignItems: 'flex-start',
    flexDirection:'row',
    flexWrap:'wrap'
  },
  Icon: {
    marginRight:20,
    marginTop:5
  },
  Text: {
    color:'orange',
    fontFamily: 'OpenSans',
    fontSize:35
  },
  switchWrapper: {
    alignItems: 'flex-end'
  },
  headerText: {
    color:'#ffffff',
    backgroundColor:"#005999",
    paddingLeft:10,
    paddingRight:10,
    fontSize:25
  },
  headerTextIndivid:{
    color:'#ffffff',
    backgroundColor:"#39acff",
    paddingLeft:10,
    paddingRight:10,
    fontSize:25,
    width:width
  },
  list: {
    flex:0
  },
  rowWrapper: {
    alignItems: 'center',
    borderBottomColor:'#dddddd',
    borderBottomWidth:1
  },
  image: {
    width:30,
    height:30,
    marginTop:7
  },
  titleWrapper: {
    marginLeft:10,
    marginRight:20
  },
  mainText: {
    fontSize:20,
    color:"#000000"
  },
  subText: {
    fontSize:15,
    color:"#dddddd"
  },
  pickerWrapper: {
    fontSize:35,
    fontFamily: 'OpenSans'
  },
  fullWidth:{
    marginTop:10,
    flexDirection:'row',
    flexWrap:'wrap',
    alignItems: 'center',
    marginBottom:10,
    width:width
  },
  picker: {
    width: 200,
    flex:1
  },
  touchable:{
    width:width
  },
  item:{
    fontFamily: 'OpenSans',
    color: '#a9a9a9',
    fontSize:15
  },
  btnBackground:{
    flexDirection:'row',
    flexWrap:'wrap',
    alignItems: 'center',
    flex:0,
    paddingLeft:width*.35,
    width:width-20,
    marginLeft:10,
    marginRight:10,
    marginTop:20,
    marginBottom:20,
    backgroundColor: 'orange',
    borderRadius:5
  },
  btnText:{
    color:'#ffffff',
    fontFamily: 'OpenSans',
    fontSize:30,
    padding:10,
    textAlign:'center'
  }
});
module.exports = ChatConfirmScreen;