'use strict';

require('node-jsx').install({extension: '.jsx'});

var jsdom = require('jsdom');

global.document = jsdom.jsdom();
global.window = document.defaultView;
global.navigator = {userAgent: []};

