'use strict';

var pretty = require('html').prettyPrint;

var React = require('react');
var ReactDOM = require('react-dom');
var ReactTestUtils = require('react-addons-test-utils');
var $ = require('jquery');


var cmp = require('./../components/pages/reviews/Item.jsx');
var model = require('./item.json');
var Wizard = require('./../components/pages/Templates/Wizard.jsx');
var wizardSchema = require('./../components/componentsFactory/wizard_schema.jsx');


var click = function (el) {
    var e = document.createEvent('MouseEvents');
    e.initMouseEvent('click', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, 0, null, null);
    el.dispatchEvent(e);
};


describe('main', function () {
    var dom, Api, props;

    describe('filled review Item', function () {

        beforeAll(function () {
            Api = {
                getReview: function (id, callback) {
                    callback(JSON.parse(JSON.stringify(model)));
                }
            };

            window.Api = Api;
            window.pageXOffset = 0;
            window.pageYOffset = 0;

            props = {
                params: {id: 12}
            };

            document.body.innerHTML = '<div></div>';

            dom = ReactDOM.render(React.createElement(cmp, props), document.body.firstChild);
        });

        it('inits - questions', function () {
            var expects = model.answers.map(function (obj) {
                return (obj.bool ? ['on', 'off'] : ['off', 'on'])
                    .map(item => 'button-' + item)
                    .map(item => 'small-button' + ' ' + item);
            });

            var classes = [].map.call(document.querySelectorAll('.knowledge ul li .question'), function (el) {
                return [].map.call(el.querySelectorAll('input[type="button"]'), function (el) {
                    return el.className;
                });
            });

            var query = document.querySelectorAll('.knowledge ul li .question input[type="button"]');
            var buttonsCls = [].map.call(query, el => el.className);

            expect(buttonsCls[0]).toBe('small-button button-on');
            expect(buttonsCls[1]).toBe('small-button button-off');

            expect(buttonsCls[2]).toBe('small-button button-off');
            expect(buttonsCls[3]).toBe('small-button button-on');

            expect(buttonsCls[4]).toBe('small-button button-off');
            expect(buttonsCls[5]).toBe('small-button button-on');

            expect(buttonsCls[6]).toBe('small-button button-off');
            expect(buttonsCls[7]).toBe('small-button button-on');
        });

        it('inits - selections', function () {
            var questions = model.knowledge.questions;
            var answers = model.answers;

            var items = [].map.call(document.querySelectorAll('.select-rectangle .description'), item => item);

            expect(items.length).toBe(model.answers.filter(a => !a.bool).length);
            expect(items[0].innerHTML).toBe(questions[1].answers[answers[1].index].text);
            expect(items[1].innerHTML).toBe(questions[2].answers[answers[2].index].text);
            expect(items[2].innerHTML).toBe(questions[3].answers[answers[3].index].text);
        });

        it('inits - answers', function () {
            var result = [].map.call(document.querySelectorAll('.question .answers'), function (el) {
                return [].map.call(el.querySelectorAll('input'), function (el) {
                    return el.checked;
                });
            });

            result.unshift([false, false, false]);

            var expects = model.knowledge.questions.map(function (question) {
                var answer = model.answers.filter(a => a.questionId === question._id).pop();
                return question.answers.map(function (answr) {
                    return answer.answerId === answr._id;
                });
            });

            expect(result).toEqual(expects);
        });

        it('clicks radio inputs', function () {
            var arr = [
                [0, 0, 1],
                [0, 1, 0],
                [1, 0, 0]
            ];

            [].forEach.call(document.querySelectorAll('.answers'), function (el, index) {
                [].forEach.call(el.querySelectorAll('input[type="radio"]'), function (el, i) {
                    var bool = Boolean(arr[index][i]);
                    if (!bool) return;

                    var e = document.createEvent('MouseEvents');
                    e.initMouseEvent('click', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, 0, null, null);

                    el.dispatchEvent(e);
                });
            });

            var selectionExpects = [];

            var expects = dom.state.item.knowledge.questions.map(function (question, i) {
                return question.answers.map(function (answer) {
                    var bool;

                    if (answer._id === dom.state.answers[i].answerId) {
                        bool = 1;
                        selectionExpects.push(answer.text);
                    } else {
                        bool = 0;
                    }

                    return bool;
                });
            });

            var selectionDescriptions = [].map.call(document.querySelectorAll('.select-rectangle .description'), function (el) {
                return el.innerHTML;
            });

            expects.shift();

            expect(expects).toEqual(arr);
            expect(selectionExpects).toEqual(selectionDescriptions);
        });

        it('moves review selections', function () {
            var item = document.querySelectorAll('.select-rectangle')[1];

            var selection;

            selection = dom.state.answers[2].selection;

            expect(selection.top).toBe(230);
            expect(selection.left).toBe(100);

            var e = document.createEvent('MouseEvents');
            e.initMouseEvent('mousedown', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, 0, null, null);
            item.dispatchEvent(e);

            var e = document.createEvent('MouseEvents');
            e.initMouseEvent('mousemove', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, 0, null, null);
            e.pageX = 10;
            e.pageY = 10;
            item.dispatchEvent(e);

            var e = document.createEvent('MouseEvents');
            e.initMouseEvent('mouseup', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, 0, null, null);
            item.dispatchEvent(e);

            selection = dom.state.answers[2].selection;

            expect(selection.top).toBe(10);
            expect(selection.left).toBe(10);
        });
    });


    describe('empty review Item', function () {
        var view;

        beforeAll(function () {
            window.Api = {
                getReview: function (id, callback) {
                    var reviewModel = JSON.parse(JSON.stringify(model));
                    reviewModel.answers = [];

                    callback(reviewModel);
                }
            };

            window.pageXOffset = 0;
            window.pageYOffset = 0;

            var props = {
                params: {id: 12}
            };

            document.body.innerHTML = '<div></div>';

            view = ReactDOM.render(React.createElement(cmp, props), document.body.firstChild);
        });

        it('new answer and selection', function () {
            document.getElementById('scrollBox').scrollTop = 0;

            var question = document.body.querySelectorAll('.knowledge .question')[0];

            var button = question.querySelectorAll('input[type="button"]')[1];
            click(button);

            var radio = document.body.querySelectorAll('.knowledge .question .answers input')[0]
            click(radio);

            var answer = view.state.answers[0];

            expect(answer.selection).toEqual({width: 200, height: 50, top: 100, left: 100});

        });

        it('move selection and add new', function () {
            var item = document.querySelectorAll('.select-rectangle')[0];

            var selection;

            selection = view.state.answers[0].selection;

            var e = document.createEvent('MouseEvents');
            e.initMouseEvent('mousedown', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, 0, null, null);
            item.dispatchEvent(e);

            var e = document.createEvent('MouseEvents');
            e.initMouseEvent('mousemove', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, 0, null, null);
            e.pageX = 10;
            e.pageY = 10;
            item.dispatchEvent(e);

            var e = document.createEvent('MouseEvents');
            e.initMouseEvent('mouseup', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, 0, null, null);
            item.dispatchEvent(e);

            selection = view.state.answers[0].selection;

            expect(selection.top).toBe(10);
            expect(selection.left).toBe(10);

            // ================================================================

            var question = document.body.querySelectorAll('.knowledge .question')[1];

            var button = question.querySelectorAll('input[type="button"]')[1];
            click(button);

            var radio = question.querySelectorAll('.answers input')[0];
            click(radio);

            expect(view.state.answers.length).toBe(2);

            // ================================================================

            var item = document.querySelectorAll('.select-rectangle')[0];

            var selection;

            selection = view.state.answers[0].selection;

            var e = document.createEvent('MouseEvents');
            e.initMouseEvent('mousedown', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, 0, null, null);
            item.dispatchEvent(e);

            var e = document.createEvent('MouseEvents');
            e.initMouseEvent('mousemove', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, 0, null, null);
            e.pageX = 10;
            e.pageY = 10;
            item.dispatchEvent(e);

            var e = document.createEvent('MouseEvents');
            e.initMouseEvent('mouseup', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, 0, null, null);
            item.dispatchEvent(e);

            selection = view.state.answers[0].selection;

            expect(view.state.answers[0].selection.top !== view.state.answers[1].selection.top).toBeTruthy();
        });
    });


    describe('wizard', function () {

        var wizard;
        var allData = {};

        beforeAll(function () {
            window.Api = {
                getReview: function (id, callback) {
                    var reviewModel = JSON.parse(JSON.stringify(model));
                    reviewModel.answers = [];

                    callback(reviewModel);
                }
            };

            window.pageXOffset = 0;
            window.pageYOffset = 0;

            var props = {
                doc: {
                    documentType: {},
                    body: {
                        sideA: 'сторона А',
                        sideB: 'сторона Б',
                        obligation: 'йцу',
                        debts: [
                            { desc: 'долг 1', date: '2014-02-01T21:00:00.000Z', toPay: '4000' }
                        ]
                    }
                },
                template: {
                    steps: wizardSchema.steps
                },
                onStepChange: function (cmp, data, stepInc, isLastStep) {
                    Object.assign(allData, data);

                    if (!isLastStep) cmp.changeStep(stepInc);
                }
            };

            document.body.innerHTML = '<div></div>';

            wizard = ReactDOM.render(React.createElement(Wizard, props), document.body.firstChild);

            jasmine.clock().install();
        });

        var fill = function (cmp, value) {
            ReactTestUtils.Simulate.change(cmp, {target : {value : value}});
        };

        it('renders correcty', function () {
            var inputs = document.querySelectorAll('fieldset input');

            expect(inputs[0].value).toBe('сторона А');
            expect(inputs[1].value).toBe('сторона Б');
            expect(inputs[2].value).toBe('йцу');
        });

        it('step 0', function () {
            var cmps = ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'input');


            fill(cmps[1], 'Сторона АА');
            fill(cmps[2], 'Сторона ББ');
            fill(cmps[3], 'Чето там про документ');

            ReactTestUtils.Simulate.click(ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'button')[0]);

            expect(allData).toEqual({
                sideA: 'Сторона АА',
                sideB: 'Сторона ББ',
                obligation: 'Чето там про документ',
                debts: [
                    { desc: 'долг 1', date: '2014-02-01T21:00:00.000Z', toPay: '4000' }
                ]
            });
        });

        it('step 1', function () {
            var cmps = ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'input');

            fill(cmps[0], 'рас долг');
            fill(cmps[1], '02.07.2015');
            fill(cmps[2], '1000');

            fill(cmps[4], 'два долг');
            fill(cmps[5], '05.10.2015');
            fill(cmps[6], '2000');

            ReactTestUtils.Simulate.click(ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'button')[1]);

            expect(JSON.parse(JSON.stringify(allData))).toEqual({
                sideA: 'Сторона АА',
                sideB: 'Сторона ББ',
                obligation: 'Чето там про документ',
                debts: [
                    { desc: 'рас долг', date: '2015-07-01T21:00:00.000Z', toPay: '1000' },
                    { desc: 'два долг', date: '2015-10-04T21:00:00.000Z', toPay: '2000' }
                ]
            });
        });

        it('step 2', function () {
            var cmps = ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'select');

            ReactTestUtils.Simulate.change(cmps[0], {target : {selectedIndex : 15}});
            ReactTestUtils.Simulate.change(cmps[1], {target : {selectedIndex : 7}});
            ReactTestUtils.Simulate.change(cmps[2], {target : {selectedIndex : 2}});

            ReactTestUtils.Simulate.click(ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'button')[1]);

            var calcDate = allData.calcDate;
            expect(calcDate.getDate()).toBe(16);
            expect(calcDate.getMonth()).toBe(7);
            expect(calcDate.getFullYear()).toBe(2014);

            expect(Object.keys(allData)).toEqual([
                'sideA', 'sideB', 'obligation',
                'debts',
                'calcDate', 'claimDate', 'limPeriod'
            ]);
        });

        it('step 3', function () {
            var inputs = document.querySelectorAll('fieldset input');
            var e = document.createEvent('MouseEvents');
            e.initMouseEvent('click', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, 0, null, null);
            inputs[1].dispatchEvent(e);

            ReactTestUtils.Simulate.click(ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'button')[1]);

            expect(allData.doCalcInflationLosses).toBe(inputs[1].value);
        });

        it('step 4', function () {
            var checkbox = document.querySelector('input[type="checkbox"]');
            var blockIf = document.querySelector('.block.if');

            expect(checkbox.checked === true).toBeTruthy();
            expect(blockIf.innerHTML).not.toBe('');

            ReactTestUtils.Simulate.change(
                ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'input')[1],
                {target : {value : 5}}
            );

            ReactTestUtils.Simulate.change(
                ReactTestUtils.findRenderedDOMComponentWithTag(wizard, 'select'),
                {target: {value: 'день'}}
            );

            expect(document.querySelector('select').value).toBe('день');

            ReactTestUtils.Simulate.change(
                ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'input')[0],
                {target : {checked : false}}
            );

            expect(checkbox.checked === false).toBeTruthy();
            expect(blockIf.innerHTML).toBe('');

            ReactTestUtils.Simulate.change(
                ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'input')[0],
                {target : {checked : true}}
            );

            ReactTestUtils.Simulate.click(ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'button')[1]);

            expect(allData.fundsPercents).toEqual({percents: 5, period: 'день' });
            expect(allData.doCalcPercents).toBeTruthy();
        });

        it('step 5', function () {
            //  тут проверяется что defaultValue сработает и отрисуется if блок под чекбоксом
            expect(document.querySelector('.if').innerHTML !== '').toBeTruthy();

            var inputs = ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'input');

            var input1 = inputs[1];
            var input2 = inputs[2];

            var radio1 = inputs[3];
            var radio2 = inputs[4];
            var radio3 = inputs[5];
            var radio4 = inputs[6];

            ReactTestUtils.Simulate.change(input1, {target: {value: 3}});
            ReactTestUtils.Simulate.change(input2, {target: {value: 7}});

            //  при переключении радио-батонов блок Switch перерисовывается
            ReactTestUtils.Simulate.change(radio1, {target: {checked: true}});
            var str = wizardSchema.steps[5].markers[1].markers[3].cases.nbu.markers[0].name.ru;
            expect(document.querySelector('.switch').innerHTML.indexOf(str) !== -1).toBeTruthy();

            ReactTestUtils.Simulate.change(radio2, {target: {checked: true}});
            expect(document.querySelector('.switch').innerHTML.indexOf('из расчета') !== -1).toBeTruthy();


            //  выбираем снова первый радио-баттон и вписываем чето в инпут
            ReactTestUtils.Simulate.change(radio1, {target: {checked: true}});
            var inputs = ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'input');
            ReactTestUtils.Simulate.change(inputs.slice(-1).pop(), {target: {value: 30}});

            ReactTestUtils.Simulate.click(ReactTestUtils.scryRenderedDOMComponentsWithTag(wizard, 'button')[1]);

            expect(allData.specialStatuteTerm).toBe(3);
            expect(allData.fineEndTerm).toBe(7);
            expect(allData.fineMultiple).toBe(30);
        });
    });
});