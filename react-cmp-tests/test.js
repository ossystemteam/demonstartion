'use strict';

var Jasmine = require('jasmine');
var jasmine = new Jasmine();


jasmine.loadConfig({
    spec_dir: 'js/tests',
    spec_files: [
        'main-spec.js',
    ],
    helpers: ['/helper.js']
});

jasmine.execute();