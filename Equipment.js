import React, { Component } from 'react';
import {Translate} from 'react-redux-i18n';
import Paper from 'material-ui/Paper';
import {red500,orange500,green500,grey500,grey300 as backgroundGreyColor,grey700 as darkGrey, white} from 'material-ui/styles/colors';
import {I18n} from 'react-redux-i18n';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import ExpandMore from 'material-ui/svg-icons/navigation/expand-more';
import theme from '../../../theme';

import DashboardEquipment,{ChannelWilma} from '../../../Services/Dashboard/equipment';
import {EQUIPMENT_STATUS_ACTIVE,EQUIPMENT_STATUS_NOT_ACTIVE,EQUIPMENT_STATUS_DISCONNECTED} from '../../../constants';

export default class Equipment extends Component{
    alertColor = orange500;
    alarmColor = red500;
    selectColor = theme.palette.primary2Color;
    activeColor = green500;
    disabledColor = grey500;
    notActiveColor = red500;
    healthIndex = 'healthIndex';
    oee = 'oee';
    constructor(props){
        super(props);
        this.getName = this.getName.bind(this);
        this.getHealth = this.getHealth.bind(this);
        this.getTopBorder = this.getTopBorder.bind(this);
        this.getBottomBorder = this.getBottomBorder.bind(this);
        this.getStyle = this.getStyle.bind(this);
        this.getValue = this.getValue.bind(this);
        this.isActive = this.isActive.bind(this);
        this.isConnected = this.isConnected.bind(this);
        this.state = {selectMenu:this.healthIndex}
    }
    static defaultProps = {
        id:'',
        name:'',
        healthIndex:'',
        alert:false,
        alarm:false,
        active:false,
        current:false,
        connected: false
    };
    static propTypes = {
        id: React.PropTypes.string.isRequired,
        name: React.PropTypes.string.isRequired,
        healthIndex: React.PropTypes.number.isRequired,
        OEEglobal: React.PropTypes.number,
        alert: React.PropTypes.bool.isRequired,
        alarm: React.PropTypes.bool.isRequired,
        active: React.PropTypes.oneOfType([React.PropTypes.bool, React.PropTypes.number]).isRequired,
        connected: React.PropTypes.oneOfType([React.PropTypes.bool, React.PropTypes.number]).isRequired,
        current: React.PropTypes.bool.isRequired,
        onTouchTap:React.PropTypes.func,
        equipment:React.PropTypes.instanceOf(DashboardEquipment)
    };
    isActive(){
        if(this.props.active)
            return true;
        else
            return false;
    }
    isConnected(){
        if(this.props.connected)
            return true;
        else
            return false;
    }
    getName(){
        return this.props.name;
    }
    getHealth(){
        let healthIndCol = this.getColor( this.props.healthIndex );
        return(
        <div style={{paddingTop: '0.5em'}} >
            <span style={{
                    fontFamily: 'Roboto',
                    fontSize: '0.75em',
                    letterSpacing: '0.015em',
                    color: '#727272'
                }} >
                <Translate value="health_index"/>:
            </span>
            <div style={{
                paddingTop: '0.125em',
                fontFamily : 'Roboto',
                fontWeight : 'bold',
                letterSpacing : '0.0175em',
                color : healthIndCol
                }}>
                <span style={{
                    fontSize : '0.875em'
                }}>
                    {this.props.healthIndex}%
                </span>
            </div>
        </div>
        );
    }

    getOEE(){
        let color = this.getColor( this.props.OEEglobal );
        return(
            <div style={{paddingTop: '0.5em'}} >
            <span style={{
                fontFamily: 'Roboto',
                fontSize: '0.75em',
                letterSpacing: '0.015em',
                color: '#727272'
            }} >
            <Translate value="oee_global"/>:
            </span>
                <div style={{
                    paddingTop: '0.0125em',
                    fontFamily : 'Roboto',
                    fontWeight : 'bold',
                    color : color,
                    letterSpacing : '0.0175em'
                }}>
                    <span
                        style={{
                            fontSize : '0.875em'
                        }}>
                        {this.props.OEEglobal}%
                    </span>
                </div>
            </div>
        );
    }
    getValue(){
        switch(this.state.selectMenu){
            case this.oee:
                return this.getOEE();
            case this.healthIndex:
            default:
                return this.getHealth();
        }
    }
    getColor( index ){

        if ( index >= 80 ) {
            return green500;
        }
        else if ( index >= 40 ) {
            return orange500;
        }
        else {
            return red500;
        }
    }

    getTopBorder(){
        let {alarm, alert} = {...this.props};
        if(alarm){
            return '0.25em solid '+this.alarmColor;
        }
        else if(alert){
            return '0.25em solid '+this.alertColor;
        }
        else {
            return 'none';
        }
    }
    getBottomBorder(){
        if(this.props.current){
            return '0.25em solid '+this.selectColor;
        }
        else {
            return 'none';
        }
    }
    getStyle(){
        return {
            borderBottom:this.getBottomBorder(),
            borderTop:this.getTopBorder(),
            height:'5em',
            padding: '0.5em 0.3125em',
            fontFamily: 'Roboto',
            fontSize: '0.75em',
            letterSpacing: '0.015em'
        }
    }
    getMenu(){
        let self = this;
        return(
            <IconMenu
                style={{
                    position: 'absolute',
                    top: '0px',
                    left: '0px'
                }}
                iconButtonElement={<IconButton><ExpandMore /></IconButton>}
                onClick={function (event) {
                    event.stopPropagation();
                }}
                onTouchTap={function (event) {
                    event.stopPropagation();
                }}
            >
                <MenuItem primaryText={I18n.t('health_index')} onTouchTap={function () {
                    self.setState({selectMenu:self.healthIndex})
                }}/>
                <MenuItem primaryText={I18n.t('oee_global')} onTouchTap={function () {
                    self.setState({selectMenu:self.oee})
                }}/>
            </IconMenu>
        );
    }
    getActiveStatus(){
        return <i style={{ color:this.activeColor, fontSize: '0.6em', marginLeft: '0.5em'}} className="zmdi zmdi-circle"></i>;
    }
    getNotActiveStatus(){
        return <i style={{ color:this.disabledColor, fontSize: '0.6em', marginLeft: '0.5em'}} className="zmdi zmdi-circle"></i>;
    }
    getDisconnectedStatus(){
        return <i style={{ color:this.notActiveColor, fontSize: '0.6em', marginLeft: '0.5em'}}></i>;
    }
    getStatus(){
        const equipment = this.props.equipment;
        if(equipment && equipment instanceof DashboardEquipment) {
            switch(equipment.getStatus()){
                case EQUIPMENT_STATUS_ACTIVE:
                    return this.getActiveStatus();
                case EQUIPMENT_STATUS_NOT_ACTIVE:
                    return this.getNotActiveStatus();
                case EQUIPMENT_STATUS_DISCONNECTED:
                default:
                    return this.getDisconnectedStatus();
            }
        }
        else return this.getDisconnectedStatus();
    }
    render(){
        let {name,id} = {...this.props};
        if(!name || name.length < 1){
            name = id;
        }
        return(
            <Paper style={this.getStyle()} className="pointer" onTouchTap={this.props.onTouchTap}>
                <div style={{width:'10%',float:'left'}}>
                    {this.getStatus()}
                </div>
                <div  style={{float: 'left', width:'60%'}}>
                    <div style={{ letterSpacing: '0.015em' }} >
                        <span style={{
                            fontFamily: 'Roboto',
                            fontSize: '0.75em',
                            color: '#212121'}}>
                            {name}
                        </span>
                    </div>
                    {this.getValue()}
                </div>
                <div style={{ float: 'left', position: 'relative', width:'10%'}}>
                    {this.getMenu()}
                </div>

            </Paper>
        )
    }
}
export class EquipmentWilma extends Equipment{
    backgroundDisableColor = backgroundGreyColor;
    disabledColor = darkGrey;
    static propTypes = {
        ...Equipment.propTypes,
        channels:React.PropTypes.arrayOf(React.PropTypes.instanceOf(ChannelWilma)),
        currentChannel: React.PropTypes.instanceOf(ChannelWilma)
    };
    constructor(props){
        super(props);
        this.getChannels = this.getChannels.bind(this);
    }
    isEqualChannels(ch1,ch2){
        if(ch1 instanceof ChannelWilma && ch2 instanceof ChannelWilma)
            return ch1.getId() === ch2.getId();
        else
             return JSON.stringify(ch1) === JSON.stringify(ch2);
    }
    getChannels(){
        let channels = [];
        if(this.props.channels && this.props.channels.length > 0) {
            const self = this;

            // let name = null;
            let i = 1;
            this.props.channels.map(function (channel, index) {
                let {current} = {...self.props};
                if(current)
                    current = self.isEqualChannels(channel, self.props.currentChannel);

                // let channelName = name + ' ' + i;
                let channelName = null;
                let chanStyle = null;

                if ( i % 2 ){
                    channelName = I18n.t('dashboard.type.wilma.S1');
                    chanStyle = { borderRight: '1px solid rgb(224, 224, 224)' }
                }else{
                    channelName = I18n.t('dashboard.type.wilma.S2');
                    chanStyle = { borderLeft: '1px solid rgb(224, 224, 224)' }
                }

                channels.push(
                    <Channel key={index} style={ chanStyle } current={current} name={channelName} onTouchTap={function () {
                        self.props.onTouchTap(channel, index);
                    }}/>
                );
                i++;
            });
        }
        return channels;
    }
    getStyle(){
        let style = super.getStyle();
        return {
            ...style,
            borderBottom:'none',
            borderTop:'none',
            height:'8.2rem',
            padding: '0px',
            fontSize: '1rem'
        };
    }
    getActiveStatus(){
        return <span>{I18n.t('status') + ': '}<span
            style={{color: this.activeColor}}>{I18n.t('connected')}</span></span>;
    }
    getNotActiveStatus(){
        return <span>{I18n.t('status') + ': '}<span
            style={{color: this.activeColor}}>{I18n.t('not_active')}</span></span>;
    }
    getDisconnectedStatus(){
        return <span>{I18n.t('status') + ': '}<span>{I18n.t('disconnected')}</span></span>
    }
    render(){
        let {name,id} = {...this.props};
        if(!name || name.length < 1){
            name = id;
        }
        let style = {
            height:'6rem',
            fontFamily: 'Roboto',
            fontSize: '1rem',
            lineHeight: '24px',
            letterSpacing: '0.16px',
            paddingLeft: '0.875rem',
            paddingRight: '0.875rem',
            borderBottom: '1px Solid rgb(224, 224, 224)'
        };
        if(!this.isConnected()){
            style = {...style, backgroundColor:this.backgroundDisableColor, color:this.disabledColor};
        }
        return(
            <Paper style={this.getStyle()}>
                <div style={style}>
                    <p style={{ margin: '0px', padding: '0.5rem 0rem' }} >{name}</p>
                    <p style={{ margin: '0px', padding: '0.8rem 0rem 0.2rem 0rem' }} >{this.getStatus()}</p>
                </div>

                <div>
                    {this.getChannels()}
                </div>

            </Paper>
        )
    }
}

export class Channel extends React.Component{
    static propTypes = {
        name:React.PropTypes.string.isRequired,
        onTouchTap:React.PropTypes.func.isRequired,
        current: React.PropTypes.bool
    };
    static style = {
        width: '50%',
        height: '2.2rem',
        paddingTop: '0.2em',
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontSize: '1rem',
        lineHeight: '24px',
        letterSpacing: '0.16px',
        color: '#727272'
};

    render(){
        const {name,current, style} = {...this.props};
        let currentStyle = {};
        if(current)
            currentStyle = {backgroundColor:theme.palette.primary2Color, color:white}
        return(
            <div
                className="pointer col-xs-6 col-sm-6 col-md-6 col-lg-6"
                style={{ ...Channel.style, ...style, ...currentStyle }}
                onTouchTap={this.props.onTouchTap}>
                {name}
            </div>
        );
    }
}