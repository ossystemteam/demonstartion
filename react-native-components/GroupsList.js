'use strict'

import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  ListView
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'




export default class GroupsList extends Component {

  static propTypes = {
    items: React.PropTypes.array.isRequired,
    onItemPress: React.PropTypes.func.isRequired
  }

  constructor (props) {
    super(props)

    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => {
        return (r1._id !== r2._id) || (r1.title !== r2.title) || (r1.phones.length !== r2.phones.length)
      }
    })

    this.state = {
      dataSource: dataSource.cloneWithRows(props.items)
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(nextProps.items)
    })
  }

  renderRow (rowData) {
    const view = {
      padding: 10,
      flexDirection: 'row',
      alignItems: 'center'
    }

    const meta = {
      fontSize: 12,
      color: 'grey'
    }

    return (
      <TouchableHighlight underlayColor="#ccc" onPress={() => this.props.onItemPress(rowData)}>
        <View style={view}>
          <View style={{flex: 1}}>
            <Text>{rowData.title}</Text>
            <Text style={meta}>{rowData.phones.length + ' contacts'}</Text>
          </View>
          <View>
            <Icon name="angle-right" size={24} color="grey" />
          </View>
        </View>
      </TouchableHighlight>
    )
  }

  renderSeparator (sectionId, rowId) {
    return (
      <View key={'' + sectionId + rowId} style={{borderWidth: 0.3, borderColor: 'grey'}} />
    )
  }

  renderHeader () {
    const view = {
      padding: 10,
      backgroundColor: 'aquamarine'
    }

    return (
      <View style={view}>
        <Text>My Groups (is this header needed?)</Text>
      </View>
    )
  }

  render () {
    return (
      <ListView
        dataSource={this.state.dataSource}
        // renderHeader={this.renderHeader.bind(this)}
        renderRow={this.renderRow.bind(this)}
        renderSeparator={this.renderSeparator.bind(this)}
      />
    )
  }
}