'use strict'

import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import AlphabetListView from 'react-native-alphabetlistview'




class Cell extends Component {

  // state = {
  //   selected: false
  // }

  // XonSelect () {
  //   const selected = !this.state.selected

  //   this.props.onSelect(this.props.item, selected)
  //   this.setState({selected})
  // }

  render () {
    const iconProps = {
      name: this.props.item.selected ? 'check-circle' : 'plus-circle',
      color: this.props.item.selected ? 'green' : '#ccc'
    }

    const style = {
      marginHorizontal: 15,
      height: 45,
      justifyContent: 'center',
      flexDirection: 'row',
      alignItems: 'center'
    }

    return (
      <TouchableOpacity underlayColor="#ccc" onPress={() => this.props.onSelect(this.props)}>
        <View style={style}>
          <View style={{flex: 1}}>
            <Text>{this.props.item.title}</Text>
            <Text style={{color: 'grey'}}>
              {(Math.round(Math.random() * 30)) + ' contacts'}
            </Text>
          </View>
          <View>
            {this.props.item.selected && (
              <Icon size={30} name="check-circle" color="green" />
            )}
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

class SectionItem extends Component {
  render () {
    return (
      <Text style={{color:'#000'}}>{this.props.title}</Text>
    )
  }
}


class SectionHeader extends Component {

  render() {
    // inline styles used for brevity, use a stylesheet when possible
    var textStyle = {

      // flex: 1,
      // borderWidth: 1
    }

    var viewStyle = {
      backgroundColor: '#eee',
      paddingHorizontal: 15,
      alignItems: 'center',
      flexDirection: 'row',
      height: 25,
    }
    return (
      <View style={viewStyle}>
          <Text style={textStyle}>{this.props.title}</Text>
      </View>
    )
  }
}




export default class ContactsPicker extends Component {

  static propTypes = {
    onCellSelect: React.PropTypes.func.isRequired,
    contacts: React.PropTypes.array.isRequired
  }

  constructor(props, context) {
    super(props, context)

    this.state={
      selectedContacts: [],
      data: this.groupContacts(props.contacts)
    }

    this.onCellSelect = this.onCellSelect.bind(this)
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      data: this.groupContacts(nextProps.contacts)
    })
  }

  groupContacts (contacts) {
    const out = {}

    contacts = contacts.slice().sort((a, b) => {
      if (a.title.toLowerCase() < b.title.toLowerCase()) return -1
      if (a.title.toLowerCase() > b.title.toLowerCase()) return 1
      return 0
    })

    contacts.forEach(contact => {
      const letter = contact.title.substr(0, 1).toUpperCase()
      out[letter] = out[letter] || []
      out[letter].push(contact)
    })

    return out
  }

  onCellSelect (cellData) {
    this.props.onCellSelect(cellData.item)
  }

  XonCellSelect (item, selected) {
    const selectedContacts = this.state.selectedContacts

    if (selected) {
      selectedContacts.push(item)
    } else {
      selectedContacts.splice(selectedContacts.indexOf(item), 1)
    }

    this.props.onCellSelect(selectedContacts)
    this.setState({selectedContacts})
  }

  render() {
    return (
      <AlphabetListView
        automaticallyAdjustContentInsets={false}
        data={this.state.data}
        cell={Cell}
        cellHeight={45}
        sectionListItem={SectionItem}
        sectionHeader={SectionHeader}
        sectionHeaderHeight={25}
        onCellSelect={this.onCellSelect}
      />
    )
  }
}