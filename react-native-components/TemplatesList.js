'use strict';

import React, {Component} from 'react';
// import ReactNative from 'react-native';
import ScrollableTabView, { ScrollableTabBar, DefaultTabBar} from 'react-native-scrollable-tab-view';
import Icon from 'react-native-vector-icons/FontAwesome';

import {
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  ScrollView,
  View,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Image,
  ListView
} from 'react-native';


export default class TemplatesList extends Component {

  static propTypes = {
    onItemPress: React.PropTypes.func.isRequired
  }

  constructor (props) {
    super(props)

    // alert(props.templates[0].isFavorite)

    this.state = {
      // categories: this.groupTemplates(props.templates)

    }
  }

  groupTemplates (templates) {
    const groups = []
    const favourites = []

    templates.forEach(item => {
      if (!item.parentCategory) {
        groups.push({
          _id: item._id,
          title: item.title,
          items: []
        })
      }
    })

    templates.forEach(item => {
      if (!item.parentCategory) return
      if (item.isFavorite) favourites.push(item)
      groups.find(g => g._id == item.parentCategory).items.push(item)
    })

    if (favourites.length > 0) {
      groups.unshift({
        _id: 'favs',
        title: 'Favourites',
        items: favourites
      })
    }

    return groups
  }

  XgroupTemplates (templates) {
    const categories = [];

    templates.forEach(template => {
      let category = categories.find(item => item.title === template.category);
      if (!category) {
        category = {title: template.category, templates: []};
        categories.push(category);
      }
      category.templates.push(template);
    });

    return categories;
  }

  renderTabBar () {
    return (
      <ScrollableTabBar
        style={{borderBottomWidth: 0}}
      />
    )
  }

  render () {
    const groups = this.groupTemplates(this.props.templates)

    return (
      <ScrollableTabView
        renderTabBar={() => this.renderTabBar()}
        tabBarBackgroundColor="white"
        style={{backgroundColor: 'white'}}
        tabBarInactiveTextColor="grey"
        tabBarActiveTextColor="orange"
        tabBarUnderlineStyle={{backgroundColor: 'red', height: 0}}
        >
        {groups.map(group => (
          <MainGrid
            key={group.title}
            tabLabel={group.title}
            items = {group.items}
            onItemClick = {this.props.onItemPress}
            toggleFavourite={this.props.toggleFavourite}
          />
        ))}
      </ScrollableTabView>
    )

    return (
      <MainGrid
        items = {this.props.templates}
        onItemClick = {this.props.onItemPress}
        toggleFavourite={this.props.toggleFavourite}
      />
    )
  }
}



var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height


class MainGrid extends Component {

  static propTypes = {
    items: React.PropTypes.array,
    onItemClick: React.PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => {
        return r1._id !== r2._id || r1.isFavorite !== r2.isFavorite
      }
    });

    this.state = {
      dataSource: ds.cloneWithRows(props.items)
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(nextProps.items),
      selectedFavorite: nextProps.selectedFavorite
    })
  }

  renderRow (rowData) {
    const imageWrap = {
      borderRadius: 10,
      overflow: 'hidden',
    }

    const view = {
      backgroundColor: 'white',
      marginLeft: 4
    }

    return (
      <View style={view}>
        <TouchableOpacity onPress={(event) => this.props.onItemClick(rowData)}>
          <View elevation={5} style={styles.row}>
            <TouchableOpacity style={styles.starIcon} onPress ={()=>this.props.toggleFavourite(rowData)}>
              <Icon name="star" size={30} color={rowData.isFavorite ? 'gold' :  'grey'} />
            </TouchableOpacity>
            <View style={imageWrap}>
              <Image style={styles.image} source={{uri: rowData.imageSrc}} ></Image>
            </View>
            <Text  style={styles.item} >{rowData.title}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <ListView
        automaticallyAdjustContentInsets={true}
        contentContainerStyle={styles.list}
        dataSource={this.state.dataSource}
        renderRow={this.renderRow.bind(this)}
      />
    );
  }
}

const styles = StyleSheet.create({
  list: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    // marginTop: 2,
    // marginLeft: 5,
    // marginRight: 5,
  },
  row:{
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 5,
    marginRight: 5,
    // backgroundColor: '#F6F6F6',
    alignItems: 'center'
  },
  image:{
    width: (width/2)-16,
    height:(height/3)-50
  },
  item: {
    width: (width/2)-20,
    fontSize: 14,
    fontFamily: 'OpenSans',
    // backgroundColor: '#ffffff',
    // textAlign: 'center',
    color: 'black'
  },
  starIcon: {
    position: 'absolute',
    backgroundColor: 'transparent',
    top: 5,
    right: 5,
    zIndex: 99
  }
});

